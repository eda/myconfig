{-# LANGUAGE TypeSynonymInstances, MultiParamTypeClasses, DeriveDataTypeable #-}

import XMonad hiding ( (|||) )
import XMonad.Core
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.StackSet as XS

import XMonad.Layout.DragPane
import XMonad.Layout.Grid
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Combo
import XMonad.Layout.StackTile
-- import XMonad.Layout.ToggleLayouts
import XMonad.Layout.MultiToggle
import XMonad.Layout.TwoPane
import XMonad.Layout.Reflect
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Layout.Named (named)

import XMonad.Actions.CycleWS
import XMonad.Actions.GroupNavigation

import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.SetWMName

import XMonad.Util.Types
import XMonad.Util.EZConfig

import Graphics.X11.ExtraTypes.XF86 as XF86

import System.IO
import System.Posix.Unistd (getSystemID, nodeName)
import qualified Data.Map as M

-- import System.IO

workspaces' = ["1:main", "2:web", "3:eclipse", "4:latex", "5:mail", "6:tmp"]
modMask' = mod4Mask

data DOUBLE = DOUBLE deriving (Read, Show, Eq, Typeable)

instance Transformer DOUBLE Window where
  transform _ x k = k (dragPane Vertical 0.03 0.5) (const x)

data FULL = FULL deriving (Read, Show, Eq, Typeable)

instance Transformer FULL Window where
  transform _ x k = k Full (const x)

layout' = smartBorders $ avoidStruts $
              mkToggle (FULL ?? DOUBLE ?? EOT) $
--              toggleLayouts Full $
--              toggleLayouts pane $
              onWorkspace "1:main" Grid $
              onWorkspace "2:web" webLayout $
              onWorkspace "3:eclipse" webLayout $
              onWorkspace "4:latex" latexLayout $
	      Grid
--              reflectHoriz (Grid *|* Full)
  where
--    webLayout = combineTwo (TwoPane 0.03 0.5) Full Full
--    webLayout = combineTwo (TwoPane 0.03 0.5) oTall oTall
    webLayout = oTall
    latexLayout = oTall
    -- latexLayout = pane ||| oTall
    -- latexLayout = dragPane Vertical 0.03 0.5 ||| oTall
    pane = named  "dragPane" $ dragPane Vertical 0.03 0.5
    oTall = Tall 1 0.03 0.5
    zTall = Tall 0 0.03 0.5
--    testLayout = Tall 1 0.03 0.5 ***||* Full

-- namedWS :: WSType
-- namedWS  = WSIs $ return $ (\x -> (XS.tag x) `elem` workspaces')

newKeys =
  [ ((modMask', xK_f), sendMessage $ Toggle FULL)
  , ((modMask' .|. shiftMask, xK_f), sendMessage $ Toggle DOUBLE)
  , ((modMask', xK_b), sendMessage ToggleStruts)
  , ((modMask', xK_Left), prevWS) -- moveTo Prev namedWS)
  , ((modMask', xK_Right), nextWS) --moveTo Next namedWS)
  , ((modMask', xK_h), prevWS) -- moveTo Prev namedWS)
  , ((modMask', xK_l), nextWS) --moveTo Next namedWS)
  , (((modMask' .|. controlMask), xK_Left), shiftToPrev) 
  , (((modMask' .|. controlMask), xK_Right), shiftToNext)
  , ((modMask', xK_w), spawn "firefox")
   , ((modMask', xK_m), spawn "thunderbird")
    , ((modMask', xK_e), spawn "emacs")
    , ((modMask', xK_j), spawn "eclipse")
  , ((modMask', xK_p), spawn "dmenu_run -fn '10x20'")
  , (((modMask' .|. controlMask), xK_Tab), nextMatch History (return True))
  , (((modMask' .|. controlMask), xK_l), spawn "gnome-screensaver-command -l")
  , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
  , ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 5%-")
  , ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 5%+")
  ]

myPP h = def
    { ppOutput          = hPutStrLn h
    , ppCurrent         = dzenColor "red" "black"
-- don't display current layout
    , ppOrder           = \(x:_:z) -> x:z
--    , ppHidden          = dzenColor "#e5e5e5" "#000000"
-- show workspace without windows
--    , ppHiddenNoWindows = id
    , ppHiddenNoWindows = dzenColor "#444444" "#000000"
--    , ppUrgent          = dzenColor "#ff0000" "#000000". dzenStrip
    , ppTitle           = id -- shorten 80
    , ppWsSep           = "  "
    , ppSep             = "  |  "
    }

-- myPP' h = def { ppOutput = hPutStrLn h }

logHook' h = dynamicLogWithPP $ myPP h

-- updateStack without changing the master
updateStack :: XS.StackSet i l a s sd -> XS.StackSet i l a s sd
updateStack = XS.modify' $ 
  \c -> case c of
     XS.Stack t [] (r:rs) -> XS.Stack t [r] rs
     otherwise            -> c

manageHook' = composeOne
  [ className =? "Iceweasel" -?> doShift "web"
  , className =? "Gimp" -?> doFloat
  , isFullscreen -?> doFullFloat
  , return True -?> doF updateStack
  ] -- <+> manageDocks

myXmonadBar = "dzen2 -dock -w 1900 -ta l -e 'onstart=lower'"
myStatusBar =
  "conky -c ~/.xmonad/conky_dzen |" ++
  "dzen2 -dock -x 1900 -w 1900 -ta r -e 'onstart=lower'"

data Host = Desktop | Netbook

getHost :: IO Host
getHost = do
  host <- nodeName `fmap` getSystemID
  return $ case host of
             "yo" -> Netbook
             _    -> Desktop

main = do
  host <- getHost
  dzenLeftBar <- spawnPipe myXmonadBar
  spawnPipe myStatusBar
  xmonad $ docks $ def
           { workspaces = workspaces'
           , modMask = modMask'
           , layoutHook = layout'
           , manageHook = manageHook'
           , logHook = logHook' dzenLeftBar >> historyHook
           , startupHook = setWMName "LG3D"
           } `additionalKeys` newKeys
