MAIL=/var/spool/mail/$USER

export SHELL USER MAIL HOST

# Le chemin dans lequel chercher les applications
PATH=\
/bin:\
$HOME/bin:\
/usr/local/bin:\
/usr/bin:\
/usr/bin/X11:\
/usr/games:\
$HOME/opt/mupad/share/bin:
export PATH

# Le chemin dans lequel chercher les pages de man
MANPATH=\
$HOME/man:\
/usr/local/man:\
/usr/X11R6/man:\
/usr/man/fr:\
/usr/man
export MANPATH
unset MANPATH

# Le r�pertoire dans lequel chercher les pages d'info
INFOPATH=\
$HOME/info:\
/usr/local/info:\
/usr/share/info
export INFOPATH

# La locale d�finit comment se comportent les diff�rentes applications
# vis-�-vis des langues �trang�res.
#if [ x"$LC_CTYPE" != x"en_US.UTF-8" ]
#then
#	LC_CTYPE=en_US
#	export LC_CTYPE
#fi

LC_CTYPE=en_US.UTF-8
export LC_CTYPE

# Avec �a, certaines applications, surtout celles des machines Linux,
# se mettront magiquement � parler votre langue
LANG=$LC_CTYPE
export LANG

# La base de donn�es terminfo contient ka description de la pluspart des
# types de terminal; il en existe deux versions, qui diff�rent l�g�rement.
# En outre, certaines applications veulent que la variable COLORTERM soit
# d�finie pour �tre en couleur.
TERMINFO=/usr/lib/terminfo
COLORTERM=
TERM=xterm-256color
export TERMINFO COLORTERM TERM

# EDITOR est le nom de l'�diteur de textes qu'utilisent diverses
# applications. Attention: ce doit �tre un �diteur en mode texte.
# On met le chemin complet pour �viter quelques acc�s disque.
# EDITOR=vim (pas la peine de chercher, rien d'autre n'a �t� trafiqu�)
EDITOR=vi
export EDITOR

# PAGER est le nom du programme � utiliser pour afficher un texte et
# permettre de le lire confortablement.
PAGER=less
export PAGER

# Les options de less: ligne d'�tat bavarde, recherche insensible aux
# majuscules/minuscules et absence de bips intempestifs.
LESS="-Miq"

# Le jeu de caract�re que doit afficher less, il corespond � la police
# utilis�e dans les terminaux
LESSCHARSET=latin1

# Par d�faut, les fichiers cr��s sont en lecture libre (mais �criture 
# interdite)
umask 022

# Param�tres IRC
#IRCSERVER=irc.mutu.net
#IRCNICK=eda
#export IRCSERVER IRCNICK

# Param�tres TeX
TEXMFHOME=$HOME/.texmf
export TEXMFHOME
#TEXINPUTS=".:$HOME/.tex//:/var/lib/texmf//:"
#export TEXINPUTS

# couleurs de ls
[[ -f ~/.dir_colors ]] && eval `dircolors ~/.dir_colors`

EMAIL=eda@mutu.net
export EMAIL

#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=ibus
#export QT_IM_MODULE=ibus

export GEM_PATH=$HOME/.gems
