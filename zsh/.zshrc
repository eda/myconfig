### options

# Quand un fichier existe d?j?, on ne peut pas l'?craser avec un >
setopt noclobber

# Les programmes lanc?s en arri?re plan ne doivent pas ?tre moins
# prioritaires
setopt nobgnice

# Zsh permet de jouer avec les dernieres commandes, avec le caractere !;
# ca conduit parfois a des gags, et l'utilisation n'est pas evidente, on
# vire
setopt nobanghist

# On ne stoque pas deux fois la meme ligne dans l'historique
setopt histignoredups

# Mais on nettoie l'historique des espaces superflus
setopt hist_reduce_blanks

# On ne veut pas que Ctrl-S et Ctrl-Q aient leur fonction habituelle de
# bloquer le terminal. Si un terminal pose probleme a ce niveau, c'est
# ici
setopt noflowcontrol                                          

# Je veux voir le resultat de mes commandes                   
setopt printexitvalue

# Et le glob etendu, parce que c'est bien
setopt extendedglob

# Pas de beep
setopt nobeep

## completions

# Quand il n'y a pas de tel fichier, pas de bip
setopt nolistbeep

# Pour un affichage plus compact des possibilites
setopt listpacked                                              


setopt INTERACTIVE_COMMENTS # to use comments in terminal   
setopt SHARE_HISTORY # share history between terminals

autoload -U compinit

compinit

fpath=(~/.zshfunctions $fpath)

#zstyle ':completion:*' menu long=yes select

#zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS} 		
#zstyle ':completion:*:*:cd:*:*' directories			

### variables

watch=(notme)
cdpath=(.)

HISTFILE=~/.zhistory
HISTSIZE=100000
HISTFILESIZE=100000
SAVEHIST=100000
export HISTFILE HISTSIZE SAVEHIST HISTFILESIZE

PS1='%n@%m:%~$ '
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

### alias

alias ll='ls -l'
alias ls='ls --color=auto'
alias lla='ls -alh'

alias uterm="'xterm' -u8 -fn -misc-fixed-medium-r-normal--15-140-75-75-c-90-iso10646-1 -sl 500"
#alias xterm='xterm -rv'
alias configure='./configure --prefix=/usr/local/stow/$(basename $(pwd))'
alias xirssi="'xterm' -rv -name irssi -e irssi"
#alias gnus='emacs -name gnus -f gnus'
#alias mpg123='mpg123 -o oss'
#alias ogg123='ogg123 -d oss'
alias spam="mutt -f \=spam"
alias vkeybd='vkeybd --device alsa --addr 17:0'
alias iown='sudo chown eda.eda'
alias gamma='ssh -t sas ssh -t gamma6 LC_CTYPE=$LC_CTYPE zsh'
alias oggenc='oggenc -b 192'
alias vm="~/vmware/vm"
alias am='alsamixer'
alias grep='grep --color=always'
alias maxbright='echo 4882 >! /sys/class/backlight/intel_backlight/brightness'
alias midbright='echo 2441 >! /sys/class/backlight/intel_backlight/brightness'
alias midbrightmore='echo 3663  >! /sys/class/backlight/intel_backlight/brightness'
alias midbrightless='echo 1221  >! /sys/class/backlight/intel_backlight/brightness'

muttf ()
{
  	if [ $# -eq 0 ]
	then
	  1=mbox
	fi
	mutt -f \=$1
}

alias cdid='. cdid'
#alias us='setxkbmap us;xmodmap ~/.Xmodmap'
alias us='setxkbmap us -option compose:menu'
alias fr='setxkbmap fr -option compose:menu'
alias mypax="pax -rw -L -s '/[:,?]//g' -s '/�/o/g' -s '/�/n/g' -s '/[��]/i/g'"
alias utf8='export LC_ALL=en_US.UTF-8'
### keys

bindkey -e

bindkey "^?" backward-delete-char
bindkey "^[[3~" delete-char
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line
bindkey "^[[5~" up-line-or-history
bindkey "^[[6~" down-line-or-history
bindkey "^[[h" beginning-of-line
bindkey "^[[f" end-of-line
bindkey "^[[g" beginning-of-line
bindkey "^[[e" beginning-of-line
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[G" beginning-of-line
bindkey "^[[E" beginning-of-line
bindkey "^[[s" beginning-of-line

bindkey '^R' history-incremental-search-backward
bindkey '^S' history-incremental-search-forward
bindkey '^K' kill-line
bindkey '^G' send-break
bindkey '^Q' push-line-or-edit
bindkey -M vicmd ':' execute-named-cmd

### misc

mesg n
umask 022

export ANSIBLE_NOCOWS=1
